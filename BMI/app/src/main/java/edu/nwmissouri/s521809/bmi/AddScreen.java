package edu.nwmissouri.s521809.bmi;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.nwmissouri.s521809.bmi.Utils.BConstants;
import mehdi.sakout.fancybuttons.FancyButton;

public class AddScreen extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private FancyButton cmBTN;
    private FancyButton kgBTN;
    private EditText heightET;
    private EditText weightET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_screen);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(AddScreen.this);

        heightET = (EditText) findViewById(R.id.add_heightET);
        heightET.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    heightET.setTextSize(30);
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    double height_in_inches;
                    double height_in_foot;
                    double exp_foot_in_inches;

                    switch (cmBTN.getText().toString()) {
                        case "CM":
                            double height_in_cm = Double.parseDouble(heightET.getText().toString());

                            edit.putFloat(BConstants.HEIGHT, (float) height_in_cm);
                            edit.apply();

                            height_in_inches = height_in_cm * (0.3937008);
                            height_in_foot = height_in_inches * (0.08333333);
                            exp_foot_in_inches = (height_in_foot - Math.floor(height_in_foot)) * 12;

                            heightET.setText(String.format("%s(%s ft, %s in)", height_in_cm, Math.floor(height_in_foot), Math.floor(exp_foot_in_inches)));
                            break;
                        case "M":
                            double height_in_m = Double.parseDouble(heightET.getText().toString());

                            height_in_cm = height_in_m * 100;
                            SharedPreferences.Editor edi = sharedPreferences.edit();
                            edi.putFloat(BConstants.HEIGHT, (float) height_in_cm);
                            edi.apply();

                            height_in_inches = height_in_cm * (0.3937008);
                            height_in_foot = height_in_inches * (0.08333333);
                            exp_foot_in_inches = (height_in_foot - Math.floor(height_in_foot)) * 12;

                            heightET.setText(String.format("%s(%s ft, %s in)", height_in_m, Math.floor(height_in_foot), Math.floor(exp_foot_in_inches)));
                            break;
                    }
                }
            }
        });

        weightET = (EditText) findViewById(R.id.add_weightET);

        cmBTN = (FancyButton) findViewById(R.id.add_cmBTN);
        cmBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((FancyButton) v).getText().equals("CM")) {
                    cmBTN.setText("M");
                    kgBTN.setText("LB");
                } else {
                    cmBTN.setText("CM");
                    kgBTN.setText("KG");
                }
            }
        });

        kgBTN = (FancyButton) findViewById(R.id.add_kgBTN);
        kgBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((FancyButton) v).getText().equals("KG")) {
                    kgBTN.setText("LB");
                    cmBTN.setText("M");
                } else {
                    kgBTN.setText("KG");
                    cmBTN.setText("CM");
                }
            }
        });

        FancyButton addBTN = (FancyButton) findViewById(R.id.add_addBTN);
        addBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (heightET == null || weightET == null) {
                    Toast.makeText(AddScreen.this, "Enter all fields", Toast.LENGTH_LONG).show();
                } else {

                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    float weight = Float.parseFloat(weightET.getText().toString());

                    edit.putFloat(BConstants.WEIGHT, weight);
                    edit.putString(BConstants.CM_OR_M, cmBTN.getText().toString());
                    edit.putString(BConstants.KG_OR_LB, kgBTN.getText().toString());
                    edit.apply();

                    finish();
                }
            }

        });

    }
}