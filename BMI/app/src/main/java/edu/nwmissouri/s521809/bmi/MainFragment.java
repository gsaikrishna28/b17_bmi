package edu.nwmissouri.s521809.bmi;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import edu.nwmissouri.s521809.bmi.Utils.BConstants;
import mehdi.sakout.fancybuttons.FancyButton;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {

    private SharedPreferences sharedPreferences;
    private TextView bmiTV;


    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container, false);
        bmiTV = (TextView) view.findViewById(R.id.bmiValueTV);
        FancyButton addBTN = (FancyButton) view.findViewById(R.id.frahment_main_addDetailsBTN);

        addBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddScreen.class));
            }
        });

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        // Inflate the layout for this fragment
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

        String kgOrLb = sharedPreferences.getString(BConstants.KG_OR_LB, "KG");
        String cmOrM = sharedPreferences.getString(BConstants.CM_OR_M, "CM");

        float height = sharedPreferences.getFloat(BConstants.HEIGHT, 160);
        float weight = sharedPreferences.getFloat(BConstants.WEIGHT, 55);
        double height_in_inches;
        float bmi = 0.0f;

        switch (kgOrLb) {
            case "KG":
                switch (cmOrM) {
                    case "CM":
                        float heightInM = height / 100;
                        Log.d("aa", "CM _KG");
                        bmi = (weight / (heightInM * heightInM));
                        break;
                    case "M":
                        Log.d("aa", "M_KG");
                        bmi = (weight / (height * height));
                        break;
                }
                break;

            case "LB":
                switch (cmOrM) {
                    case "CM":
                        Log.d("aa", "CM_LB");
                        height_in_inches = height * (0.3937008);
                        bmi = (float) (((weight) / (height_in_inches * height_in_inches)) * 703);
                        break;
                    case "M":
                        height_in_inches = height * (0.3937008);
                        bmi = (float) (((weight) / (height_in_inches * height_in_inches)) * 703) ;
                        break;
                }
                break;
        }

        bmiTV.setText(String.format("%d", (int) bmi));

    }
}
