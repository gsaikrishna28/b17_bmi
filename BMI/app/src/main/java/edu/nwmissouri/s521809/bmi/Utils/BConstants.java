package edu.nwmissouri.s521809.bmi.Utils;

public class BConstants {

    public static String isFirstLaunch = "IS_FIRST_LAUNCH";
    public static String NAME = "Name";
    public static String AGE = "Age";
    public static String HEIGHT = "Height";
    public static String WEIGHT = "Weight";
    public static String GENDER = "Gender";
    public static String CM_OR_M = "Cm_Or_M";
    public static String KG_OR_LB = "Kg_Or_Lb";
}
