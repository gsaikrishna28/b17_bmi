package edu.nwmissouri.s521809.bmi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import edu.nwmissouri.s521809.bmi.Utils.BConstants;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

        if (!(sharedPreferences.getBoolean(BConstants.isFirstLaunch, false))) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            WelcomeScreenFragment fragment = new WelcomeScreenFragment();
            transaction.add(R.id.mainContainer, fragment);
            transaction.commit();
        } else {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            MainFragment fragment = new MainFragment();
            transaction.add(R.id.mainContainer, fragment);
            transaction.commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if ((sharedPreferences.getBoolean(BConstants.isFirstLaunch, false))) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            MainFragment fragment = new MainFragment();
            transaction.add(R.id.mainContainer, fragment);
            transaction.commit();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();


    }


    public void addBTN(View view) {
        Intent intent = new Intent(this, AddDetails.class);
        startActivity(intent);
    }
}
