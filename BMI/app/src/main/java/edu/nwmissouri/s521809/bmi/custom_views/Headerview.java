package edu.nwmissouri.s521809.bmi.custom_views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import edu.nwmissouri.s521809.bmi.R;

/**
 * Created by SaiKrishna on 11/17/2015.
 */
public class Headerview extends RelativeLayout{

    public Headerview(Context context) {
        super(context);
        init();
    }

    public Headerview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Headerview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutInflater.inflate(R.layout.header_view, this);
    }
}
